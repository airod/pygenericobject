# PyGenericObject

Transformez vos données json ou dictionnaire en objet Python

Dépendances :
   - json

Compatibilités :
   Python 2.7
   Python 3.x

Installation :
   Copiez le dossier PyGenericObject directement dans votre installation des librairies de votre Python
   ex : C:\\Python39\Lib\site-packages

Architecture :

	utiliser PyGenericObject avec un dictionnaire Python ou un objet Json
	PyGenericObject va determiner tout seul si les datas sont un dictionnaire python ou un objet json
	En entrée si les datas sont un objet json, celui ci est convertit en dictionnaire python

	PyGenericObject détermine pour chaque item des datas leur type :
	   - si c'est un dictionnaire, alors il en crée un nouveau PyGenericObject
       - si c'est une liste, il crée un PyGenericListItem
	   - si c'est une string, il teste si elle est de type objet Json.
	        - si c'est une liste, il crée un PyGenericListItem
			- sinon il crée un PyGenericObject
       - sinon c'est un string, int, bool, ..., il crée un PyGenericFinalItem

    Au final, le plus petit object sera toujours un **PyGenericFinalItem**

Exemple :

 cette entrée : 
 ```
 {'a':'toto', 'b':{'a':'toto', 'b':1}, 'c':[{'a':'toto', 'b':1},{'c':'toti', 'b':3}]}
 ```
 donnera :
  ```
  PyGenericObject {
        'a': PyGenericFinalItem,
				'b': PyGenericObject{
						'a':PyGenericFinalItem,
						'b':PyGenericFinalItem
					},
				'c': PyGenericListItem[
							PyGenericObject{'a':'PyGenericFinalItem', 'b':PyGenericFinalItem},
							PyGenericObject{'c':'PyGenericFinalItem', 'b':PyGenericFinalItem}
					]
		}
  ```
Usage :
```
import PyGenericObject as pgo

myData = {
    "isActive": False,
    "balance": "$3,028.27",
    "age": 36,
    "eyeColor": "blue",
    "name": "Thompson Reid",
    "address": "937 Montague Terrace, Bodega, District Of Columbia, 1942",
    "registered": "2016-05-18T04:17:26 -02:00",
    "latitude": 26.472837,
    "longitude": 135.584212,
    "tags": [
      "est",
      2,
      "commodo"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Carmen Mcpherson"
      },
      {
        "id": 1,
        "name": "Gomez Suarez"
      }
    ],
    "greeting": "Hello, Thompson Reid! You have 4 unread messages.",
    "favoriteFruit": "apple"
  }

myDataObject = pgo.PyGenericObject(myData)

print(myDataObject.tags)
print(myDataObject.tags.getItems())
print(myDataObject.tags.getItem(0))
print(myDataObject.tags.getItem(0).get())
print(myDataObject.tags.getItem(0).getType())

print(myDataObject.friends)
print(myDataObject.friends.getItems())
print(myDataObject.friends.getItem(0))
print(myDataObject.friends.getItem(0).get('id').get())
print(myDataObject.friends.getItem(0).get('id').getType())

myDataObject.friends.getItem(0).get('id').set(125)
print(myDataObject.friends.getItem(0).get('id').get())

print(myDataObject.friends.getItems())
myDataObject.friends.setItem(22)
print(myDataObject.friends.getItems())
print(myDataObject.friends.getItem(2).get())

myDataObject.friends.getItem(0).set('Pseudo','airod')
print(myDataObject.friends.getItem(0).Pseudo.get())
```



Sortie :
```
<PyGenericObject.PyGenericListItem object at 0x000000D1E96E0F88>
[<PyGenericObject.PyGenericFinalItem object at 0x000000D1E96E36C8>, <PyGenericObject.PyGenericFinalItem object at 0x000000D1E96E3708>, <PyGenericObject.PyGenericFinalItem object at 0x000000D1E96E3748>]
<PyGenericObject.PyGenericFinalItem object at 0x000000D1E96E36C8>
est
<class 'str'>
<PyGenericObject.PyGenericListItem object at 0x000000D1E96E3788>
[<PyGenericObject.PyGenericObject object at 0x000000D1E96E37C8>, <PyGenericObject.PyGenericObject object at 0x000000D1E96E3948>]
<PyGenericObject.PyGenericObject object at 0x000000D1E96E37C8>
0
<class 'int'>
125
[<PyGenericObject.PyGenericObject object at 0x000000D1E96E37C8>, <PyGenericObject.PyGenericObject object at 0x000000D1E96E3948>]
[<PyGenericObject.PyGenericObject object at 0x000000D1E96E37C8>, <PyGenericObject.PyGenericObject object at 0x000000D1E96E3948>, <PyGenericObject.PyGenericFinalItem object at 0x000000D1E96E38C8>]
22
airod
```

