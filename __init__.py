# coding: utf-8
#-------------------------------------------------------------------------------
# Name:        PyGenericObject
# Purpose:
#
# Author:      BENOIT MARTIRE
#
# Created:     17/11/2020
# Copyright:   (c) BENOIT MARTIRE 2020
# Licence:     GNU
#-------------------------------------------------------------------------------
import json

__version__ = 0.4
__name__ = "PyGenericObject"
__author__ = "B.Martiré"
__git__ = "https://gitlab.com/airod/pygenericobject"

class PyGenericObject(object):
    __roles__=None
    __allow_access_to_unprotected_subobjects__=1

    def __init__(self,obj,dataType="dict"):
        if type(obj)==type({}):
            self.obj = obj
        elif dataType=='json':
            import json
            self.obj = json.loads(obj)
        else:
            try :
                self.obj = json.loads(obj)
            except:
                raise Exception('Bad obj type')

        self.hystoric = []
        self.__autoconstruct__()

    def getDatas(self):
        return self.obj

    def toJson(self):
        return json.dumps(self.obj)

    def keys(self):
        return self.__dict__.keys()

    def items(self):
        return self.__dict__.items()

    def values(self):
        return self.__dict__.values()

    def __autoconstruct__(self):
        for k,v in self.obj.items():
            self.set(k,v, hystorize=False)

    def __itemPyGenericObject__(self,v):
        if type(v)==type({}):
                return PyGenericObject(v)
        elif type(v)==type([]):
            return PyGenericListItem(v)

        elif type(v)==type(""):
            try :
                v= json.loads(v)
                if type(v)==type({}) or type(v)==type([]):
                    return self.__itemPyGenericObject__(v)
                else:
                    return self.__unitItem__(v)
            except:
                return self.__unitItem__(v)

        elif type(v) == PyGenericObject:
            return v
        else:
            return self.__unitItem__(v)

    def __unitItem__(self,item):
        return  PyGenericFinalItem(item)

    def hystorize(self):
        """Sets a version of the object data"""
        data = self.obj.copy()
        self.hystoric.append(data)

    def restore(self,idx=-1):
        """
            restore a version of the object data
            if idx does not exist in hystory, return IndexError
        """
        return "To Do"

    def get(self,k,default=None):
        """
        Return the value of data key
        """
        return self.__dict__.get(k,default)

    def set(self,k,v,hystorize=True):
        """ Add a new key,value in object
            If hytorize is False, the old data object is not added to the historic (default : True)
        """
        if hystorize: self.hystorize()
        self.__dict__[k] = self.__itemPyGenericObject__(v)
        self.obj[k] = v

    def __type__():
        return "PyGenericObject"

    def remove(self, k,hystorize=True):
        """ delete entry by key
            If hytorize is False, the old data object is not added to the historic (default : True)
        """
        if hystorize: self.hystorize()
        try :
            self.__dict__.pop(k)
            self.obj.pop(k)
        except:
            self.restore()
            raise KeyError("Unknown key in object")

class PyGenericFinalItem(object):
    def __init__(self,data):
        self.set(data)

    def set(self,data):
        self.__value__ = data
        self.__type__ = type(data)

    def get(self):
        return self.__value__

    def getType(self):
        return self.__type__

    def __type__():
        return "PyGenericFinalItem"

    def keys(self):
        return self.__dict__.keys()

    def items(self):
        return self.__dict__.items()

    def values(self):
        return self.__dict__.values()

class PyGenericListItem(object):
    def __init__(self,item):
        self.list = item
        self.items = []
        self.__autoconstruct__()

    def __autoconstruct__(self):
        for idx in range(len(self.list)):
            item = self.list[idx]
            self.setItem(item)

    def __unitItem__(self,item):
        return  PyGenericFinalItem(item)

    def getItems(self):
        return self.items

    def getItem(self,idx) :
        return self.items[idx]

    def setItem(self, item):
        if type(item)==type({}):
            item = PyGenericObject(item)
        else:
            item = self.__unitItem__(item)
        self.items.append(item)

    def __type__():
        return "PyGenericListItem"



if __name__ == '__main__':
    print("test")
    data = {"a":"b","c":{"a":"b"}, "d":[{"a":"b"},{"c":"d"}], "e":'{"a":"b"}', "f":'[{"a":"b"},{"c":"d"}]'}

    pobj = PyGenericObject(data)
    print(pobj.f.get(0).a.items())


