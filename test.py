#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      BENOIT
#
# Created:     17/11/2020
# Copyright:   (c) BENOIT 2020
# Licence:     <your licence>
#-------------------------------------------------------------------------------

from genericObject import PyGenericObject

def main():
    data = open('datas.json','r')
    myJson = data.read()

    pyObj = PyGenericObject(myJson, dataType="json")
    print(pyObj)
    ##    print(pyObj.glossary.obj)
    print(pyObj.glossary.title)
    pyObj.glossary.set('supp',{'test':True})
    print(pyObj.glossary.supp.test)

    print(pyObj.glossary.supp.test.get())
    print(pyObj.glossary.supp.test.getType())
    pyObj.glossary.supp.test.set("voici un changement de type")
    print(pyObj.glossary.supp.test.get())
    print(pyObj.glossary.supp.test.getType())

    print(pyObj.glossary.GlossDiv.GlossList.GlossEntry.GlossDef.para.get())


if __name__ == '__main__':
    main()
